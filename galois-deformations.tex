\documentclass[10pt, a4paper]{scrartcl}

\usepackage{packages}
\usepackage{commands}
\bibliography{references.bib}

\title{Deformation theory of Galois representations}
\author{Manuel Hoff}
\date{}

\begin{document}

    \maketitle

    These are notes for a talk I am giving in the Kleine AG on \emph{Modularity lifting theorems} taking place in the summer term 2023 in Bonn.
    The main reference for this talk is \cite[Section 3]{gee}.

    I thank the organizers of the seminar, Thomas Manopulo and Calle Sönne, and the other speakers, Giulio Ricci, Xiaoxiang Zhou, Paul Siemon and Reinier Sorgdrager, for helping me with the preparation, as well as Bence Forrás for finding typos.
    Also, use these notes at your own risk, they probably contain mistakes (feel free to tell me about these mistakes)!

    \begin{notation}
        Fix a rational prime $p \neq 2$.
        Let $L/\Qp$ be a finite extension contained in a fixed algebraic closure $\Qpbar$ of $\Qp$ with ring of integers $\calO$, maximal ideal $\lambda \subseteq \calO$ and residue field $\FF$.
        Write
        \[
            \calC_{\calO} \coloneqq \curlybr[\Big]{\text{complete Noetherian local $\calO$-algebras with residue field $\FF$}}.
        \]
        The letter $A$ always denotes an object in $\calC_{\calO}$.
        Note that $A$ can always be presented as a quotient
        \[
            A = \calO \doublesquarebr{x_1, \dotsc, x_n}/(f_1, \dotsc, f_m)
        \]
        for some $n, m \in \ZZ_{\geq 0}$ and power series $f_1, \dotsc, f_m \in \calO \doublesquarebr{x_1, \dotsc, x_n}$.

        Let $G$ be a profinite group.
        We assume that for every open subgroup $H \subseteq G$ we have $\abs{\Hom(H, \Fp)} < \infty$ (this is called Mazur's condition $\Phi_p$).

        Also fix a (continuous) representation $\overline{\rho} \colon G \to \GL_2(\FF)$ and a character $\chi \colon G \to \calO^{\times}$ lifting $\det(\overline{\rho})$.
    \end{notation}

    \section{Generalities}

    \subsection*{Deformations and deformation rings}

    \begin{definition}
        A \emph{framed deformation of $\overline{\rho}$ (with determinant $\chi$) to $A$} is a representation $\rho \colon G \to \GL_2(A)$ that reduces to $\overline{\rho}$ and has determinant $\chi$.
        We write
        \[
            \Def^{\square} = \Def^{\square}_{\overline{\rho}, \chi} \colon \calC_{\calO} \to \catsets
        \]
        for the functor sending $A$ to the set of framed deformations of $\overline{\rho}$ to $A$.
    
        Assume that $\overline{\rho}$ is absolutely irreducible.
        Then a \emph{deformation of $\overline{\rho}$ to $A$} is a tuple $(M, \rho)$ consisting of a free $A$-module $M$ of rank $2$ and a representation $\rho \colon G \to \Aut_A(M)$ that reduces to $\overline{\rho}$ and has determinant $\chi$.
        We write
        \[
            \Def \colon \calC_{\calO} \to \catsets
        \]
        for the functor sending $A \in \calC_{\calO}$ to the set of isomorphism classes of deformations of $\overline{\rho}$ to $A$.
    \end{definition}

    \begin{remark} \label{rmk:framed-vs-unframed-1}
        Assume that $\overline{\rho}$ is absolutely irreducible.
        There is a natural morphism $\Def^{\square} \to \Def$ that is heuristically given by \enquote{forgetting the basis of $M$}.
        Let $(M, \rho) \in \Def(A)$ and choose an $\FF$-basis $\overline{\calB}$ of $\FF \otimes_A M$ such that the composition
        \[
            G \xrightarrow{\rho} \Aut_A(M) \to \Aut_{\FF}(\FF \otimes_A M) \overset{\overline{\calB}}{\cong} \GL_2(\FF)
        \]
        agrees with $\overline{\rho}$ (such a basis is unique up to scaling because $\overline{\rho}$ is absolutely irreducible).
        Then there is a natural bijection
        \[
            \faktor{\curlybr[\big]{\text{$A$-bases $\calB$ of $M$ reducing to $\overline{\calB}$}}}{(1 + \frakm_A)} \cong \fib_{(M, \rho)}\roundbr[\big]{\Def^{\square}(A) \to \Def(A)}
        \]
        that is given by sending the equivalence class of an $A$-basis $\calB$ of $M$ to the composition
        \[
            G \xrightarrow{\rho} \Aut_A(M) \overset{\calB}{\cong} \GL_2(A). 
        \]
    \end{remark}

    \begin{theorem}[Mazur]
        The functor $\Def^{\square}$ is representable by a universal framed deformation
        \[
            \rho^{\square} \colon G \to \GL_2 \roundbr[\big]{R^{\square}}.
        \]
        When $\overline{\rho}$ is absolutely irreducible then the functor $\Def$ is representable by (the isomorphism class of) a universal deformation
        \[
            \rho^{\univ} \colon G \to \Aut_{R^{\univ}} \roundbr[\big]{M^{\univ}}.
        \]
    \end{theorem}

    \begin{remark} \label{rmk:framed-vs-unframed-2}
        Assume that $\overline{\rho}$ is absolutely irreducible.
        Then the morphism $\Def^{\square} \to \Def$ induces a morphism $R^{\univ} \to R^{\square}$.
        In fact the observation in \Cref{rmk:framed-vs-unframed-1} implies that $R^{\square}$ is formally smooth over $R^{\univ}$ of relative dimension $3 = 2^2 - 1$.
    \end{remark}

    \subsection*{Tangent spaces}

    \begin{lemma} \label{lem:tangent-space-cohomology}
        There exists a natural isomorphism of $\FF$-vector spaces
        \[
            Z^1(G, \ad^0 \overline{\rho}) \cong \Tgt \roundbr[\big]{\Def^{\square}}
        \]
        given by sending a $1$-cocycle $\phi \colon G \to \M_2(\FF)^{\tr = 0}$ to the tangent vector $(1 + \phi \varepsilon) \overline{\rho} \in \Def^{\square}(\FF[\varepsilon]) = \Tgt(\Def^{\square})$.

        When $\overline{\rho}$ is absolutely irreducible then this isomorphism induces an isomorphism
        \[
            H^1(G, \ad^0 \overline{\rho}) \cong \Tgt(\Def).
        \]
    \end{lemma}

    \begin{proof}
        Every lift of $\overline{\rho}$ to a map of sets $\rho \colon G \to \GL_2(\FF[\varepsilon])$ is of the form $\rho = (1 + \phi \varepsilon) \overline{\rho}$ for some map of sets $\phi \colon G \to \M_2(\FF)$.
        Now we can compute (for $g, h \in G$)
        \begin{align*}
            \rho(g) \rho(h) & = (1 + \phi(g) \varepsilon) \overline{\rho}(g) (1 + \phi(h) \varepsilon) \overline{\rho}(h)
            \\
            & = \overline{\rho}(g) \overline{\rho}(h) + \overline{\rho}(g) \phi(h) \overline{\rho}(h) \varepsilon + \phi(g) \overline{\rho}(g) \overline{\rho}(h) \varepsilon
            \\
            & = \roundbr[\Big]{1 + \roundbr[\big]{\phi(g) + \overline{\rho}(g) \phi(h) \overline{\rho}(g)^{-1}} \varepsilon} \overline{\rho}(gh).
        \end{align*}
        Thus we see that $\rho$ is a group homomorphism if and only if $\phi$ is a $1$-cocycle.
        We now also compute (for $g \in G$)
        \begin{align*}
            \det \roundbr[\big]{\rho(g)} &=
            \det \roundbr[\big]{1 + \phi(g) \varepsilon} \det \roundbr[\big]{\overline{\rho}(g)}
            \\
            &= \roundbr[\Big]{1 + \tr \roundbr[\big]{\phi(g)}} \chi(g).
        \end{align*}
        Thus we see that $\rho$ has determinant $\chi$ if and only if $\phi$ has image inside $\M_2(\FF)^{\tr = 0}$.
        This finishes the proof of the first claim (up to checking that the given map is $\FF$-linear).

        For proving the second claim assume that $\overline{\rho}$ is absolutely irreducible.
        Now observe that for $a \in \M_2(\FF)^{\tr = 0}$ we have (for $g \in G$)
        \begin{align*}
            (1 + a \varepsilon) \overline{\rho}(g) (1 - a \varepsilon) & = \overline{\rho}(g) + a \overline{\rho}(g) \varepsilon - \overline{\rho}(g) a \varepsilon
            \\
            & = \roundbr[\Big]{1 + \roundbr[\big]{a - \overline{\rho}(g) a \overline{\rho}(g)^{-1}} \varepsilon} \overline{\rho}(g).
        \end{align*}
        Thus we see that $\rho \in \Tgt(\Def^{\square})$ lies in the kernel of $\Tgt(\Def^{\square}) \to \Tgt(\Def)$ if and only if the corresponding $1$-cocycle $\phi \in Z^1(G, \ad^0 \overline{\rho})$ is cohomologically trivial.
    \end{proof}

    \begin{corollary} \label{cor:dimension-tangent-space}
        We have
        \[
            \dim_{\FF} \Tgt \roundbr[\big]{\Def^{\square}} = 3 - h^0(G, \ad^0 \overline{\rho}) + h^1(G, \ad^0 \overline{\rho}).
        \]
        When $\overline{\rho}$ is absolutely irreducible then we have
        \[
            \dim_{\FF} \Tgt \roundbr[\big]{\Def} = h^1(G, \ad^0 \overline{\rho}).
        \]
    \end{corollary}

    \begin{proof}
        Both claims follow from \Cref{lem:tangent-space-cohomology}, using the exact sequence of $\FF$-vector spaces
        \[
            0 \to H^0(G, \ad^0 \overline{\rho}) \to \ad^0 \overline{\rho} \to Z^1(G, \ad^0 \overline{\rho}) \to H^1(G, \ad^0 \overline{\rho}) \to 0. \qedhere
        \]
    \end{proof}

    \begin{remark}
        Assume that $\overline{\rho}$ is absolutely irreducible.
        Then we have $h^0(G, \ad^0 \overline{\rho}) = 0$ and the dimension formula in \Cref{cor:dimension-tangent-space} matches the description of $R^{\square}$ as a power series algebra over $R^{\univ}$ in $3$ variables given in \Cref{rmk:framed-vs-unframed-2}.
    \end{remark}

    \begin{proposition}
        We have
        \[
            \krull R^{\square} \in \roundbr[\big]{4 - h^0(G, \ad^0 \overline{\rho}) + h^1(G, \ad^0 \overline{\rho})} + \squarebr[\big]{-h^2(G, \ad^0 \overline{\rho}), \; 0}.
        \]
        When $h^2(G, \ad^0 \overline{\rho}) = 0$ then $R^{\square}$ is actually formally smooth over $\calO$.

        Assume that $\overline{\rho}$ is absolutely irreducible.
        We have
        \[
            \krull R^{\univ} \in \roundbr[\big]{1 + h^1(G, \ad^0 \overline{\rho})} + \squarebr[\big]{-h^2(G, \ad^0 \overline{\rho}), \; 0}.
        \]
        When $h^2(G, \ad^0 \overline{\rho}) = 0$ then $R^{\univ}$ is in fact formally smooth over $\calO$.
    \end{proposition}

    \begin{proof}
        See \cite[Lemma 3.13. and Corollary 3.14.]{gee}.
    \end{proof}

    \section{Local Galois deformations}

    \begin{notation}
        Let $K/\Ql$ be a finite extension, where $\ell$ is a rational prime (possibly equal to $p$).
        We denote by $q$ the cardinality of the residue field of $K$.
        In this section we assume that $G = G_K$ is the absolute Galois group of $K$ (with respect to a fixed algebraic closure $\overline{K}$ of $K$).

        We have the inertia subgroups $P_K \subseteq I_K \subseteq G$.
        We write $\sigma \in I_K/P_K$ for the topological generator induced by a compatible choice of prime-to-$\ell$ roots of unity in $\overline{K}$ and $\varphi \in G/P_K$ for a choice of lift of the Frobenius $\Frob_K \in G/I_K$.
        Note that we then have the relation $\varphi \sigma \varphi^{-1} = \sigma^q$.

        We also assume that $L$ is big enough.
        In the case $p = \ell$ this means that $L$ contains the image of $K$ under every embedding $K \to \Qpbar$, and in the case $p \neq \ell$ we refer to \cite[Section 3.3.]{boeckle} for the precise condition.
    \end{notation}

    \subsection*{The case $p = \ell$}

    \begin{theorem}
        Assume that $K/\Qp$ is unramified.
        For each embedding $\sigma \colon K \to \Qpbar$, let $H_{\sigma}$ be a set of two distinct integers whose difference is $\leq p - 2$.

        Then there exists a unique reduced, $p$-torsionfree quotient $R^{\square}_{\cris, (H_{\sigma})_{\sigma}}$ of $R^{\square}$ with the property that a continuous ($\calO$-linear) homomorphism $R^{\square} \to \Qpbar$ factors through $R^{\square}_{\cris, (H_{\sigma})_{\sigma}}$ if and only if the corresponding representation $G \to \GL_2(\Qpbar)$ is crystalline with Hodge-Tate weights $(H_{\sigma})_{\sigma}$.

        Finally, $R^{\square}_{\cris, (H_{\sigma})_{\sigma}}$ is formally smooth over $\calO$ of relative dimension $3 + [K:\Qp]$.
    \end{theorem}

    \subsection*{The case $p \neq \ell$}

    \begin{theorem}
        The generic fiber $\Def^{\square, \rig}$ is reduced and the union of finitely many smooth irreducible components of dimension $3$.
        The function
        \[
            \Def^{\square, \rig} \roundbr[\big]{\Qpbar} \ni \rho \mapsto \roundbr[\Big]{\restr{\WD(\rho)}{I_K} \; \text{(forgetting $N$)}}
        \]
        is constant on the irreducible components of $\Def^{\square, \rig}$ and in fact the irreducible components of $\Def^{\square, \rig}$ are in bijection with the \emph{inertial Weil-Deligne types} (by which we mean isomorphism classes of $\restr{\WD(\rho)}{I_K}$ where we do not forget the monodromy operator $N$) that come from $p$-adic Galois representations $\rho$ deforming $\overline{\rho}$.
    \end{theorem}

    \begin{definition}
        Let $\tau$ be an inertial Weil-Deligne type.
        Then we write $R^{\square}_{\tau}$ for the $p$-torsionfree quotient of $R^{\square}$ corresponding to the irreducible component of $\Def^{\square, \rig}$ enumerated by $\tau$.
        Note that $R^{\square}_{\tau}$ is a domain of Krull dimension $4$.
    \end{definition}

    \begin{proposition} \label{prop:p-not-l-distinct-eigenvalues}
        Assume  $m \coloneqq v_p(q - 1) > 0$, that $\overline{\rho}$ and $\chi$ are unramified and that
        \[
            \overline{\rho}(\varphi) = \begin{pmatrix} \overline{\alpha} & \\ & \overline{\beta} \end{pmatrix}
        \]
        with $\overline{\alpha} \neq \overline{\beta}$.
        Then the occuring inertial Weil-Deligne types are given by the following list.
        \begin{itemize}
            \item
            We have the inertial Weil-Deligne type $\ur$ (the label $\ur$ stands for \enquote{unramified}) that is given by
            \[
                r(\sigma) = \begin{pmatrix} 1 & \\ & 1 \end{pmatrix}, \qquad N = \begin{pmatrix} 0 & \\ & 0 \end{pmatrix}.
            \]
            
            \item
            For every non-trivial $p^m$-th root of unity $\zeta \in \calO$ we have the inertial Weil-Deligne type $\zeta$ that is given by
            \[
                r(\sigma) = \begin{pmatrix} \zeta & \\ & \zeta^{-1} \end{pmatrix}, \qquad N = \begin{pmatrix}0 & \\ & 0 \end{pmatrix}.
            \]
        \end{itemize}
        In all cases the corresponding deformation ring $R^{\square}_{\tau}$ is formally smooth over $\calO$ of relative dimension $3$.

        In fact, after fixing a lift $\alpha \in \calO^{\times}$ of $\overline{\alpha}$ we have an explicit description
        \[
            R^{\square} \cong \calO \doublesquarebr{x, y, B, u}/\roundbr[\big]{(1 + u)^{p^m} - 1}
        \]
        under which the universal framed deformation $\rho^{\square}$ is given by
        \begin{align*}
            \varphi & \mapsto
            \begin{pmatrix}
                1 & y \\ x & 1
            \end{pmatrix}
            \begin{pmatrix}
                \alpha + B & \\ & \chi(\varphi)/(\alpha + B)
            \end{pmatrix}
            \begin{pmatrix}
                1 & y \\ x & 1
            \end{pmatrix}^{-1},
            \\
            \sigma & \mapsto
            \begin{pmatrix}
                1 & y \\ x & 1
            \end{pmatrix}
            \begin{pmatrix}
                1 + u & \\ & (1 + u)^{-1}
            \end{pmatrix}
            \begin{pmatrix}
                1 & y \\ x & 1
            \end{pmatrix}^{-1}.
        \end{align*}
    \end{proposition}

    \begin{proposition}
        Assume that $m \coloneqq v_p(q -1) > 0$, that $\overline{\rho}$ is trivial and that $\chi$ is unramified.
        Then the occuring inertial Weil-Deligne types are given by the following list.
        \begin{itemize}
            \item
            We have the same inertial Weil-Deligne types $\ur$ and $\zeta$ for non-trivial $p^m$-th roots of unity $\zeta \in \calO$ as in \Cref{prop:p-not-l-distinct-eigenvalues}.

            \item
            We also have the inertial Weil-Deligne type $\mon$ (the label $\mon$ stands for \enquote{monodromy}) that is given by
            \[
                r(\sigma) = \begin{pmatrix} 1 & \\ & 1 \end{pmatrix}, \qquad N = \begin{pmatrix} 0 & 1 \\ & 0 \end{pmatrix}.
            \]
        \end{itemize}
        We now also denote by $R^{\square}_1$ the quotient of $R^{\square}$ such that
        \[
            \Spec \roundbr[\big]{R^{\square}_1} = \Spec \roundbr[\big]{R^{\square}_{\ur}} \cup \Spec \roundbr[\big]{R^{\square}_{\mon}},
        \]
        so that $R^{\square}_1$ parametrizes framed deformations $\rho$ of $\overline{\rho}$ such that $\rho(\sigma)$ has characteristic polynomial $(t - 1)^2$.

        We have the following properties.
        \begin{itemize}
            \item
            For a non-trivial $p^m$-th root of unity $\zeta$, we have $R^{\square}_1/\lambda R^{\square}_1 = R^{\square}_{\zeta}/\lambda R^{\square}_{\zeta}$.

            \item
            The decomposition
            \[
                \Spec \roundbr[\big]{R^{\square}_1/\lambda R^{\square}_1} = \Spec \roundbr[\big]{R^{\square}_{\ur}/\lambda R^{\square}_{\ur}} \cup \Spec \roundbr[\big]{R^{\square}_{\mon}/\lambda R^{\square}_{\mon}}
            \]
            is the decomposition of $\Spec(R^{\square}_1/\lambda R^{\square}_1)$ into irreducible components.

            \item
            $R^{\square}_{\ur}$ is formally smooth over $\calO$ of relative dimension $3$.
        \end{itemize}
    \end{proposition}

    \section{Global Galois deformations}

    \subsection*{Deformation problems}

    \begin{definition} \label{def:deformation-problem}
        A \emph{deformation problem for $\overline{\rho}$} is a (non-empty) closed subfunctor $\calD \subseteq \Def^{\square}$ such that for $A \in \calC_{\calO}$, $\rho \in \calD(A)$ and $a \in \M_2(\frakm_A)$ we have $(1 + a) \rho (1 + a)^{-1} \in \calD(A)$.
    \end{definition}

    \begin{remark}
        Here are some remarks about \Cref{def:deformation-problem}.
        \begin{itemize}
            \item
            Assume that $\overline{\rho}$ is absolutely irreducible.
            Then a closed subfunctor $\calD \subseteq \Def^{\square}$ is a deformation problem for $\overline{\rho}$ if and only if it is the preimage of a closed subfunctor $\calD' \subseteq \Def$.

            \item
            Let $\calD \subseteq \Def^{\square}$ be a deformation problem.
            Then the tangent space
            \[
                \Tgt(\calD) \subseteq \Tgt \roundbr[\big]{\Def^{\square}} \cong Z^1(G, \ad \overline{\rho})
            \]
            is the preimage of a (uniquely determined) $\FF$-subspace of $H^1(G, \ad \overline{\rho})$.

            \item
            Let $\calD \subseteq \Def^{\square}$ be a closed subfunctor that corresponds to some ideal $I \subseteq R^{\square}$ that satisfies $I = \sqrt{I}$ and $I \neq \frakm_{R^{\square}}$.
            Then, in order to check that $\calD$ is a deformation problem it suffices to check the defining condition for $(A, \rho) = (R^{\square}/I, \rho^{\square})$.

            Here is an elementary example that illustrates that things can go wrong here.
            Suppose that
            \[
                \overline{\rho} =
                \begin{pmatrix}
                    \chi_1 & \\ & \chi_2
                \end{pmatrix}
            \]
            for distinct characters $\chi_1, \chi_2 \colon G \to \FF^{\times}$.
            Then $\calD \coloneqq \Spec(\FF) \subseteq \Def^{\square}$ is not a deformation problem.
            Indeed we clearly have
            \[
                \begin{pmatrix}
                    1 & \varepsilon \\ & 1
                \end{pmatrix}
                \begin{pmatrix}
                    \chi_1 & \\ & \chi_2
                \end{pmatrix}
                \begin{pmatrix}
                    1 & - \varepsilon \\ & 1
                \end{pmatrix}
                =
                \begin{pmatrix}
                    \chi_1 & (\chi_2 - \chi_1) \varepsilon \\ & \chi_2
                \end{pmatrix} \notin \calD \roundbr[\big]{\FF[\varepsilon]}.
            \]
        \end{itemize}
    \end{remark}

    \subsection*{$T$-framed deformations of type $\calS$}

    \begin{notation}
        Let $F/\QQ$ be a finite extension and let $S$ be a finite set of places of $F$, containing all the places above $p$.
        In this section we assume that $G = G_{F, S}$ is the Galois group of the maximal extension of $F$ unramified outside of $S$ (with respect to a fixed algebraic closure $\overline{F}$ of $F$).

        For a place $v \in S$ we denote by $F_v$ the completion of $F$ at $v$ and by $G_v$ the absolute Galois group of $F_v$ (with respect to a fixed algebraic closure $\overline{F_v}$ of $F_v$).
        We also fix $F$-linear embeddings $\overline{F} \to \overline{F_v}$ that give rise to group homomorphisms $G_v \to G$.

        Now also fix deformation problems $\calD_v$ for $\restr{\overline{\rho}}{G_v}$ for every $v \in S$ as well as a subset $T \subseteq S$, and set $\calS \coloneqq (S, (\calD_v)_{v \in S})$.

        Assume that $\overline{\rho}$ is absolutely irreducible.
    \end{notation}

    \begin{definition}
        A \emph{$T$-framed deformation of $\overline{\rho}$ of type $\calS$ to $A$} is a tuple $(M, \rho, (\calB_v)_{v \in T})$, where $(M, \rho) \in \Def(A)$ is a deformation of $\overline{\rho}$ to $A$ and the $\calB_v$ are bases of $M$, such that for every $v \in S$ the representation
        \[
            G_v \xrightarrow{\restr{\rho}{G_v}} \Aut_A(M) \overset{\calB}{\cong} \GL_2(A)
        \]
        is contained in $\calD_v(A)$ (for one or equivalently every basis $\calB$ of $M$).
        We write
        \[
            \Def^{\square_T}_{\calS} \colon \calC_{\calO} \to \catsets
        \]
        for the functor sending $A$ to the set of isomorphism classes of $T$-framed deformations of $\overline{\rho}$ of type $\calS$ to $A$.
    \end{definition}

    \begin{theorem}
        The functor $\Def^{\square_T}_{\calS}$ is representable by (the isomorphism class of) a universal $T$-framed deformation of type $\calS$
        \[
            \rho^{\square_T} \colon G \to \Aut_{R^{\square_T}_{\calS}} \roundbr[\big]{M^{\square_T}}, \qquad (\calB_v^{\square_T})_{v \in T}.
        \]
        When $T$ is empty we also write $R^{\univ}_{\calS}$ for $R^{\square_T}_{\calS}$.
    \end{theorem}

    \begin{remark}
        For $v \in T$ we have a natural morphism
        \[
            \Def^{\square_T}_{\calS} \to \calD_v, \qquad \roundbr[\big]{M, \rho, (\calB_v)_{v \in T}} \mapsto \roundbr[\big]{G_v \xrightarrow{\restr{\rho}{G_v}} \Aut_A(M) \overset{\calB_v}{\cong} \GL_2(A)}
        \]
        and thus obtain a morphism $R^{\square}_{\restr{\overline{\rho}}{G_v}}/I(\calD_v) \to R^{\square_T}_{\calS}$.
        Putting these morphisms together for varying $v \in T$ we then obtain a morphism
        \[
            R^{\loc}_{\calS, T} \coloneqq \mathop{\widehat{\bigotimes}_{\calO}}_{\vphantom{\prod} v \in T} R^{\square}_{\restr{\overline{\rho}}{G_v}}/I(\calD_v) \to R^{\square_T}_{\calS}.
        \]
    \end{remark}

    \printbibliography
\end{document}