# ws2023-galois-deformations

These are notes for a talk I am giving in the Kleine AG (organized by Thomas Manopulo and Calle Sönne).
A compiled version can be found [here](https://makoba.gitlab.io/ss2023-galois-deformations/galois-deformations.pdf).